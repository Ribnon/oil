#include <oil/external/task/TaskSystem.h>

#include "../source/task/TaskSystem.cpp"

#include <limits>
#include <iostream>
#include <cstdint>


int main()
{
	uint32_t index = 42;
	uint32_t generation = 255;

	TaskId original = oil::task::TaskSystem::makeTaskId(index, generation);

	++generation;

	TaskId next = oil::task::TaskSystem::makeTaskId(index, generation);

	if(original == next)
	{
		std::cout << "modification failed" << std::endl;
		return 1;
	}
	if(oil::task::TaskSystem::generationFromTaskId(next) != 0)
	{
		std::cout << "overflow result wrong" << std::endl;
		return 1;
	}
	if(oil::task::TaskSystem::indexFromTaskId(next) != oil::task::TaskSystem::indexFromTaskId(original))
	{
		std::cout << "unintended side effect" << std::endl;
		return 1;
	}

	std::cout << "tests finished successfully" << std::endl;
	return 0;
}
