#include <oil/external/debug/assert.h>
#include <oil/external/task/TaskSystem.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct SaveThreadIdParams
{
	uint64_t* storage;
	uint32_t thread;
} SaveThreadIdParams;

void saveThreadId(void* const taskParameters)
{
	SaveThreadIdParams* params = (SaveThreadIdParams*)taskParameters;
	*params->storage = params->thread;
}

typedef struct ExpectParams
{
	uint32_t referenceValue;
	uint32_t* actualValue;
} ExpectParams;

void expect(void* const taskParams)
{
	ExpectParams const* params = (ExpectParams const*)taskParams;

	OILassert(*params->actualValue == params->referenceValue, "value mismatch");

	++(*params->actualValue);
}

int main()
{
	#define kWorkerCount 4

	WorkerThreadDescriptor workerDescriptors[kWorkerCount];
	for (size_t i = 0; i < kWorkerCount; ++i)
	{
		workerDescriptors[i].affinityMask = 1 | (1 << (i+1));
	}

	TaskSystemDescriptor systemDescriptor;
	systemDescriptor.taskCount = 4096;
	systemDescriptor.queueSize = 1024;
	systemDescriptor.workerThreadCount = kWorkerCount;
	systemDescriptor.workerThreadDescriptions = workerDescriptors;
	systemDescriptor.concurrentPendingDependencyCount = 1024;

	TaskSystem const* taskSystem = load_runtime_task_system(NULL);

	size_t memoryRequirement = taskSystem->get_system_memory_requirement(&systemDescriptor);
	void* memory = malloc(memoryRequirement);
	TaskScheduler* scheduler = taskSystem->create_task_system(&systemDescriptor, (uint8_t*)memory);

	TaskDescriptor tDesc;
	tDesc.kernel = &saveThreadId;
	tDesc.successorCount = 0;

	uint64_t workerIds[kWorkerCount] = { 0 };
	{
		TaskId taskIds[kWorkerCount] = { 0 };
		for(uint32_t i = 0; i < kWorkerCount; ++i)
		{
			SaveThreadIdParams params;
			params.storage = &workerIds[i];
			params.thread = i+1;

			tDesc.affinity = (1 << (i + 1));
			tDesc.paramter_size = sizeof(SaveThreadIdParams);
			tDesc.parameter_data = &params;
			tDesc.successorCount = 0;
			tDesc.successors = NULL;

			TaskId taskId;

			OilResult result = taskSystem->acquire_tasks(scheduler, 1, &taskId);
			if(OilSuccess != result)
			{
				printf("failed to acquire task");
				return 1;
			}

			result = taskSystem->enqueue_tasks(scheduler, 1, &taskId, &tDesc);
			if(OilSuccess != result)
			{
				printf("failed to enqueueTask");
				return 1;
			}
			taskIds[i] = taskId;
		}

		for(size_t i = 0; i < kWorkerCount; ++i)
		{
			while(OIL_FALSE == taskSystem->is_task_completed(scheduler, taskIds[i]))
			{
			}
		}

		OilBool uniqueWorkerIds = OIL_TRUE;
		for(size_t i = 0; i < kWorkerCount; ++i)
		{
			for(size_t j = i + 1; j < kWorkerCount; ++j)
			{
				if(workerIds[i] == workerIds[j])
				{
					uniqueWorkerIds = OIL_FALSE;
				}
			}
		}

		if(OIL_FALSE == uniqueWorkerIds)
		{
			printf("tasks were not distributed as intended");
			return 1;
		}
	}

	uint32_t sharedData = 0;
	{
#define sequenceLength 8
		uint32_t sequenceAffninities[sequenceLength] =
		{
			1,
			1,
			1,
			1 << 2,
			1 << 2,
			1 << 3,
			1,
			1 << 1
		};
		TaskId taskIds[sequenceLength] = { 0 };
		TaskDescriptor taskDescriptors[sequenceLength] = { 0 };

		ExpectParams params[sequenceLength] = { 0 };

		taskSystem->acquire_tasks(scheduler, sequenceLength, taskIds);

		for(uint32_t i = 0; i < sequenceLength; ++i)
		{
			TaskDescriptor* descriptor = &taskDescriptors[i];

			params[i].actualValue = &sharedData;
			params[i].referenceValue = i;

			descriptor->kernel = &expect;
			descriptor->affinity = sequenceAffninities[i];
			descriptor->parameter_data = &params[i];
			descriptor->paramter_size = sizeof(ExpectParams);

			if( i + 1 < sequenceLength)
			{
				descriptor->successorCount = 1;
				descriptor->successors = &taskIds[i+1];
			}
		}

		taskSystem->enqueue_tasks(scheduler, sequenceLength, taskIds, taskDescriptors);

		for(size_t i = 0; i < sequenceLength; ++i)
		{
			while(OIL_FALSE == taskSystem->is_task_completed(scheduler, taskIds[i]))
			{
			}
		}

		if(sharedData != sequenceLength)
		{
			printf("end result not matching");
			return 1;
		}
	}

	taskSystem->shut_down(scheduler);

	free(memory);

	return 0;
}
