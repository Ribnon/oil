#include <oil/internal/core/container/AtomicRingBuffer.h>

#include <cstdint>
#include <iostream>
#include <thread>

int main()
{
	std::cout << "starting basic testing" << std::endl;
	{
		constexpr std::size_t kElementCount = 16;
		std::uint64_t buffer[kElementCount] = {};

		oil::core::container::AtomicRingBuffer<std::uint64_t, std::uint32_t> ringBuffer(buffer, kElementCount);

		bool res = false;
		for(std::size_t i = 0; i < kElementCount; ++i)
		{
			res = ringBuffer.try_push(1, &i);
			if(false == res)
			{
				std::cout << " insertion failed" << std::endl;
				return 1;
			}
		}

		std::uint64_t elementCountCopy = kElementCount;
		res = ringBuffer.try_push(1, &elementCountCopy);
		if(true == res)
		{
			std::cout << " ringbuffer overflow" << std::endl;
			return 1;
		}

		for(std::size_t i = 0; i < kElementCount; ++i)
		{
			std::uint64_t value = ~0ull;
			res = ringBuffer.try_pop(1, &value);

			if(false == res)
			{
				std::cout << " popping failed" << std::endl;
				return 1;
			}

			if( value != i )
			{
				std::cout << " incorrect value popped" << std::endl;
				return 1;
			}
		}

		std::uint64_t dummy = 0;
		res = ringBuffer.try_pop(1, &dummy);
		if(true == res)
		{
			std::cout << " popping succeeded when it should not have" << std::endl;
			return 1;
		}
	}

	std::cout << "starting concurrency testing" << std::endl;
	{
		static constexpr std::uint32_t threadCount = 6;
		static constexpr std::uint32_t elementsPerBatch = 512;
		static constexpr std::uint32_t batchesPerThread = 1024;

		static constexpr std::uint32_t elementCount = threadCount * elementsPerBatch * batchesPerThread;

		auto threadFunction = [](oil::core::container::AtomicRingBuffer<std::uint64_t, std::uint64_t>* queue, std::uint64_t* result, std::uint32_t batches)
		{
			std::uint64_t buffer[elementsPerBatch] = {};
			for(std::uint32_t i = 0; i < batches; ++i)
			{
				while(false == queue->try_pop(elementsPerBatch, buffer))
				{

				}

				for(std::uint32_t j  = 0; j < elementsPerBatch; ++j)
				{
					*result += buffer[j];
				}
			}
		};


		std::uint64_t* buffer = (std::uint64_t*)malloc(sizeof(std::uint64_t) * elementCount);
		oil::core::container::AtomicRingBuffer<std::uint64_t, std::uint64_t> queue(buffer, elementCount);
		std::uint64_t results[threadCount] = {};
		std::thread threads[threadCount] = {};
		for(std::uint32_t i = 0; i < threadCount; ++i)
		{
			threads[i] = std::thread(threadFunction, &queue, &results[i], batchesPerThread);
		}

		std::uint64_t* inputBuffer = (std::uint64_t*)malloc(sizeof(std::uint64_t) * elementCount);
		for(std::uint64_t i = 0; i < elementCount; ++i)
		{
			inputBuffer[i] = i;
		}
		queue.try_push(elementCount, inputBuffer);

		std::uint64_t totalSum = 0;
		for(std::uint32_t i = 0; i < threadCount; ++i)
		{
			threads[i].join();
			totalSum += results[i];
			std::cout << "thread result: " << results[i] << std::endl;
		}

		std::uint64_t controlSum = 0;
		for(std::uint64_t i= 0; i < elementCount; ++i )
		{
			controlSum += i;
		}

		if(totalSum != controlSum)
		{
			std::cout << "failed, expected: " << controlSum << ", got " << totalSum << std::endl;
			return 1;
		}
		std::cout << "succeeded, expected: " << controlSum << ", got " << totalSum << std::endl;		
	}

	std::cout << "starting overflow testing" << std::endl;
	{
		using OverflowCounter = std::uint8_t;
		constexpr OverflowCounter size = 210;

		std::uint64_t* buffer = static_cast<std::uint64_t*>(malloc(sizeof(std::uint64_t) * size));
		oil::core::container::AtomicRingBuffer<std::uint64_t, OverflowCounter> queue(buffer, size);


		constexpr size_t dummieCount = 70;
		std::uint64_t const dummies[dummieCount] = {
			 1,  2,  3,  4,  5,
			 6,  7,  8,  9, 10,
			11, 12, 13, 14, 15,
			16, 17, 18, 19, 20,
			 1,  2,  3,  4,  5,
			 6,  7,  8,  9, 10,
			11, 12, 13, 14, 15,
			16, 17, 18, 19, 20,
			 1,  2,  3,  4,  5,
			 6,  7,  8,  9, 10,
			11, 12, 13, 14, 15,
			16, 17, 18, 19, 20,
			 1,  2,  3,  4,  5,
			 6,  7,  8,  9, 10
		};

		bool success = true;
		success &= queue.try_push(dummieCount, dummies);
		success &= queue.try_push(dummieCount, dummies);
		success &= queue.try_push(dummieCount, dummies);
		if(false == success)
		{
			std::cout << "pusing test elements failed." << std::endl;
			return 1;
		}

		std::uint64_t controlDummies[dummieCount];
		success = queue.try_pop(dummieCount, controlDummies);
		if(false == success)
		{
			std::cout << "poping test elements failed." << std::endl;
			return 1;
		}

		for(size_t i = 0; i < dummieCount; ++i)
		{
			if(dummies[i] != controlDummies[i])
			{
				std::cout << "poped elements have incorrect value." << std::endl;
				return 1;
			}
		}

		success = queue.try_push(dummieCount, dummies);
		if(false == success)
		{
			std::cout << "pusing test elements with overflow failed." << std::endl;
			return 1;
		}

		// no overflow here, so assume this is correct.
		success = queue.try_pop(dummieCount, controlDummies);
		success = queue.try_pop(dummieCount, controlDummies);

		success = queue.try_pop(dummieCount, controlDummies);
		if(false == success)
		{
			std::cout << "poping test elements with overflow failed." << std::endl;
			return 1;
		}

		for(size_t i = 0; i < dummieCount; ++i)
		{
			if(dummies[i] != controlDummies[i])
			{
				std::cout << "poped elements with overflow have incorrect value." << std::endl;
				return 1;
			}
		}


		success = queue.try_push(dummieCount, dummies);
		if(false == success)
		{
			std::cout << "pusing test elements after overflow failed." << std::endl;
			return 1;
		}

		success = queue.try_pop(dummieCount, controlDummies);
		if(false == success)
		{
			std::cout << "poping test elements after overflow failed." << std::endl;
			return 1;
		}

		for(size_t i = 0; i < dummieCount; ++i)
		{
			if(dummies[i] != controlDummies[i])
			{
				std::cout << "poped elements after overflow have incorrect value." << std::endl;
				return 1;
			}
		}


	}

	return 0;
}

