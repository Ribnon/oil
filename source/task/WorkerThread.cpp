#include <oil/internal/task/WorkerThread.h>

#include <oil/external/task/TaskSystem.h>

namespace oil {
namespace task {

WorkerThread::WorkerThread(const TaskSystem& system, TaskScheduler& scheduler, uint32_t const affinityMask)
	: system_(system)
	, scheduler_(scheduler)
	, filterMask_(affinityMask)
	, run_(0)
	, thread_(threadEntryPoint_, this)
{

}

WorkerThread::~WorkerThread()
{
	thread_.join();
}

void WorkerThread::stop()
{
	this->run_.store(1);
}

bool WorkerThread::stopped() const
{
	return this->run_.load() == 2;
}

void WorkerThread::work_()
{
	while(0 == this->run_.load())
	{
		system_.work_task(&scheduler_, this->filterMask_);
	}
	this->run_.store(2);
}


void WorkerThread::threadEntryPoint_(WorkerThread* worker)
{
	worker->work_();
}

}
}
