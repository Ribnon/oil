#include <oil/external/debug/assert.h>
#include <oil/external/task/TaskSystem.h>

#include <oil/internal/core/container/AtomicRingBuffer.h>
#include <oil/internal/task/WorkerThread.h>

#include <cstring>
#include <new>
#include <mutex>

struct DependencySet
{
	std::uint32_t offset : 24;
	std::uint32_t count : 8;
};

static_assert( sizeof(DependencySet) == 4, "bitfield has unexpected size");

struct TaskData
{
	TaskId id;
	std::atomic< uint32_t > pendingDependencies;
	TaskKernel kernel;
	DependencySet successors;
};

struct Task
{
	static constexpr size_t kDesiredStructSize = 64;
	TaskData taskData;

	static constexpr size_t kParameterStorageSize = kDesiredStructSize - sizeof(TaskData);
	uint8_t parameterStorage[kParameterStorageSize];
};

struct Dependency
{
	TaskId followUpTask;
};

typedef struct TaskScheduler
{
	typedef oil::core::container::AtomicRingBuffer< Task* > TaskQueue;
	typedef oil::core::container::AtomicRingBuffer< TaskId > TaskIdQueue;
	TaskIdQueue freeTasks;

	size_t taskCount;
	Task* taskStorage;

	size_t workerCount;
	oil::task::WorkerThread* workers;

	std::uint32_t successorStorageSize;
	std::mutex successorMutex;
	Dependency* successorStorage;
	std::uint8_t* successorPendingFlags;
	std::uint32_t pendingSuccesorStart;
	std::uint32_t pendingSuccessorEnd;

	size_t queueCount;
	AffinityMask* queueAffinities;
	TaskQueue* queues;
} TaskScheduler;

namespace oil {
namespace task {

struct TaskSystem
{
	static size_t get_system_memory_requirement(TaskSystemDescriptor const * taskSystemDescriptor);
	static TaskScheduler* create_task_system(TaskSystemDescriptor const * taskSystemDescriptor, uint8_t* memory);

	static OilResult acquire_tasks(TaskScheduler* scheduler, std::uint32_t taskCount, TaskId* ids);
	static OilResult release_tasks(TaskScheduler* scheduler, std::uint32_t taskCount, TaskId const * ids);
	static OilResult enqueue_tasks(TaskScheduler* scheduler, std::uint32_t taskCount, TaskId const* ids, TaskDescriptor const* descriptors);

	static OilResult work_task(TaskScheduler* scheduler, AffinityMask workerAffinity);
	static OilBool is_task_completed(TaskScheduler const* scheduler, TaskId taskId);

	static size_t get_task_parameter_storage_size();

	static void shut_down(TaskScheduler* scheduler);

	static uint32_t indexFromTaskId(TaskId id);
	static uint32_t generationFromTaskId(TaskId id);
	static TaskId makeTaskId(uint32_t index, uint32_t generation);
};

::TaskSystem const taskSystemV1 {
	&oil::task::TaskSystem::get_system_memory_requirement,
	&oil::task::TaskSystem::create_task_system,
	&oil::task::TaskSystem::acquire_tasks,
	&oil::task::TaskSystem::release_tasks,
	&oil::task::TaskSystem::enqueue_tasks,
	&oil::task::TaskSystem::work_task,
	&oil::task::TaskSystem::is_task_completed,
	&oil::task::TaskSystem::get_task_parameter_storage_size,
	&oil::task::TaskSystem::shut_down,
	{}
};

}
}

extern "C"
{
TaskSystem const* load_runtime_task_system(void const* /*runtime*/)
{
	return &oil::task::taskSystemV1;
}
}

namespace
{

size_t count_unique_affinities(TaskSystemDescriptor const & desc)
{
	size_t uniques = 0;

	for(unsigned i = 0; i < desc.workerThreadCount; ++i)
	{
		bool duplicate = false;

		for(unsigned j = 0; j < i; ++j)
		{
			if(desc.workerThreadDescriptions[j].affinityMask == desc.workerThreadDescriptions[i].affinityMask)
			{
				duplicate = true;
				break;
			}
		}

		if(false == duplicate)
		{
			++uniques;
		}
	}

	return uniques;
}

/// returns 0 all uniqes are already present in `uniques`.
AffinityMask find_next_unique_Affinity(AffinityMask* uniques, unsigned uniqueCount, AffinityMask* all, unsigned allCount)
{
	for(unsigned allIndex = 0; allIndex < allCount; ++allIndex)
	{
		bool isUnique = true;

		for(unsigned uniquesIndex = 0; uniquesIndex < uniqueCount; ++uniquesIndex)
		{
			if(all[allIndex] == uniques[uniquesIndex])
			{
				isUnique = false;
				break;
			}
		}

		if(isUnique)
		{
			return all[allIndex];
		}
	}

	return 0;
}
}

namespace oil {
namespace task {


size_t TaskSystem::get_system_memory_requirement(const TaskSystemDescriptor * taskSystemDescriptor)
{
	//TODO: consider alignments and add padding.

	size_t size = sizeof(TaskScheduler);
	size += taskSystemDescriptor->taskCount * sizeof(Task);
	size += taskSystemDescriptor->taskCount * sizeof(TaskId);

	size += taskSystemDescriptor->workerThreadCount * sizeof(oil::task::WorkerThread);

	size_t const uniqueAffinities = count_unique_affinities(*taskSystemDescriptor);
	size += uniqueAffinities * sizeof(AffinityMask);
	size += uniqueAffinities * sizeof(TaskScheduler::TaskQueue);
	size += taskSystemDescriptor->queueSize * uniqueAffinities * sizeof(Task*);

	size += taskSystemDescriptor->concurrentPendingDependencyCount * sizeof(Dependency);
	size += (taskSystemDescriptor->concurrentPendingDependencyCount + 7) / 8;

	return size;
}

TaskScheduler* TaskSystem::create_task_system(TaskSystemDescriptor const * desc, uint8_t* memory)
{
	TaskScheduler* scheduler = reinterpret_cast< TaskScheduler* >(memory);
	new (&scheduler->successorMutex) std::mutex;
	memory += sizeof(TaskScheduler);

	scheduler->taskCount = desc->taskCount;
	scheduler->taskStorage = reinterpret_cast< Task* >(memory);
	memory += sizeof(Task) * desc->taskCount;
	new (&scheduler->freeTasks) TaskScheduler::TaskIdQueue(reinterpret_cast< TaskId* >(memory), desc->taskCount);
	memory += sizeof(TaskId) * desc->taskCount;

	for(std::uint32_t i = 0; i < desc->taskCount; ++i)
	{
		Task& task = scheduler->taskStorage[i];
		task.taskData.id = makeTaskId(i, 0);
		task.taskData.pendingDependencies.store(0);
		task.taskData.kernel = nullptr;
		task.taskData.successors.count = 0;
		task.taskData.successors.offset = 0;
		scheduler->freeTasks.try_push(1, &task.taskData.id);
	}

	scheduler->workers = reinterpret_cast< WorkerThread* >(memory);
	memory += sizeof(WorkerThread) * desc->workerThreadCount;

	size_t const uniqueAffinities = count_unique_affinities(*desc);
	scheduler->queueAffinities = reinterpret_cast< AffinityMask* >(memory);
	memory += sizeof(AffinityMask) * uniqueAffinities;
	scheduler->queues = reinterpret_cast< TaskScheduler::TaskQueue* >(memory);
	memory += sizeof(TaskScheduler::TaskQueue) * uniqueAffinities;

	Task** queueStorages = reinterpret_cast< Task** >(memory);
	memory += sizeof(Task*) * uniqueAffinities * desc->queueSize;

	AffinityMask affinityBufferTemp [128] = {};
	if(desc->workerThreadCount > 128)
	{
		//TODO: move to error handling layer??
		return nullptr;
	}
	else
	{
		for(unsigned i = 0; i < desc->workerThreadCount; ++i)
		{
			affinityBufferTemp[i] = desc->workerThreadDescriptions[i].affinityMask;
		}
	}
	scheduler->workerCount = desc->workerThreadCount;

	for(unsigned i = 0; i < uniqueAffinities; ++i)
	{
		new (&scheduler->queues[i]) TaskScheduler::TaskQueue(&queueStorages[desc->queueSize * i], desc->queueSize);
		scheduler->queueAffinities[i] = find_next_unique_Affinity(scheduler->queueAffinities, i, affinityBufferTemp, desc->workerThreadCount);
	}
	scheduler->queueCount = uniqueAffinities;

	for(unsigned i = 0; i < desc->workerThreadCount; ++i)
	{
		new (&scheduler->workers[i]) WorkerThread(taskSystemV1, *scheduler, desc->workerThreadDescriptions[i].affinityMask);
	}

	scheduler->successorStorage = reinterpret_cast< Dependency* >(memory);
	memory += sizeof(Dependency) * desc->concurrentPendingDependencyCount;
	scheduler->pendingSuccesorStart = 0;
	scheduler->pendingSuccessorEnd = 0;
	scheduler->successorStorageSize = desc->concurrentPendingDependencyCount;

	scheduler->successorPendingFlags = reinterpret_cast< std::uint8_t* >(memory);
	memory += sizeof(std::uint8_t) * ((desc->concurrentPendingDependencyCount + 7) / 8);

	return scheduler;
}

OilResult TaskSystem::acquire_tasks(TaskScheduler * const scheduler, uint32_t const taskCount, TaskId* const ids)
{
	return (true == scheduler->freeTasks.try_pop(taskCount, ids)) ? OilSuccess : OilError;
}

OilResult TaskSystem::release_tasks(TaskScheduler * const scheduler, uint32_t const taskCount, TaskId const * const ids)
{
	return (true == scheduler->freeTasks.try_push(taskCount, ids)) ? OilSuccess : OilError;
}

OilResult TaskSystem::enqueue_tasks(TaskScheduler* const scheduler, std::uint32_t taskCount, TaskId const * ids, TaskDescriptor const * descriptors)
{
	for(std::uint32_t i = 0; i < taskCount; ++i)
	{
		TaskDescriptor const & descriptor = descriptors[i];

		for(std::uint32_t successorIndex = 0; successorIndex < descriptor.successorCount; ++successorIndex)
		{
			TaskId const successorId = descriptor.successors[successorIndex];
			Task* successor = &scheduler->taskStorage[indexFromTaskId(successorId)];
			if(generationFromTaskId(successor->taskData.id) != generationFromTaskId(successorId))
			{
				OILassert(generationFromTaskId(successor->taskData.id) == generationFromTaskId(successorId), "an invalid TaskId was used to specify a successor");
				continue;
			}
			++successor->taskData.pendingDependencies;
		}
	}

	for(std::uint32_t i = 0; i < taskCount; ++i)
	{

		TaskId const taskId = ids[i];
		Task* task = &scheduler->taskStorage[indexFromTaskId(taskId)];
		TaskDescriptor const & desc = descriptors[i];

		task->taskData.kernel = desc.kernel;

		size_t const maxSize = TaskSystem::get_task_parameter_storage_size();
		size_t const copyAmount = desc.paramter_size < maxSize ? desc.paramter_size : maxSize;

		for(std::uint32_t successorIndex = 0; successorIndex  < desc.successorCount; ++successorIndex)
		{
			TaskId successorId = desc.successors[successorIndex];
			Task* successorTask = &scheduler->taskStorage[indexFromTaskId(successorId)];
			OILassert(generationFromTaskId(successorTask->taskData.id) == generationFromTaskId(successorId), "referenced successor is invalid");
		}

		std::uint32_t offset = 0;
		if(desc.successorCount > 0)
		{
			std::lock_guard<std::mutex> const lg(scheduler->successorMutex);
			offset = scheduler->pendingSuccessorEnd;
			scheduler->pendingSuccessorEnd += desc.successorCount;

			for(std::uint32_t pendingSuccessorIndex = offset; pendingSuccessorIndex < scheduler->pendingSuccessorEnd; ++pendingSuccessorIndex)
			{
				std::uint32_t const byteIndex = (pendingSuccessorIndex % scheduler->successorStorageSize) / 8;
				std::uint32_t const bitIndex = offset - (byteIndex * 8);

				scheduler->successorPendingFlags[byteIndex] |= (1 << bitIndex);
			}
		}

		for(std::uint32_t successorIndex = 0; successorIndex < desc.successorCount; ++successorIndex)
		{
			scheduler->successorStorage[offset + successorIndex].followUpTask = desc.successors[successorIndex];
		}

		task->taskData.successors.offset = offset;
		task->taskData.successors.count = desc.successorCount;

		memcpy(task->parameterStorage, desc.parameter_data, copyAmount);

		bool enqueued = false;

		for(unsigned queueIndex = 0; queueIndex < scheduler->queueCount; ++queueIndex)
		{
			unsigned const wrappedQueueIndex = (queueIndex + indexFromTaskId(taskId)) % scheduler->queueCount;
			if(0 != (scheduler->queueAffinities[wrappedQueueIndex] & desc.affinity))
			{
				while(false == scheduler->queues[wrappedQueueIndex].try_push(1, &task))
				{
					//try again
				}
				enqueued = true;
				break;
			}
		}

		if(false == enqueued)
		{
			uint32_t generation = generationFromTaskId(task->taskData.id);
			uint32_t index = indexFromTaskId(task->taskData.id);
			task->taskData.id = makeTaskId(index, generation + 1);

			while(false == scheduler->freeTasks.try_push(1, &task->taskData.id))
			{

			}
		}
	}
	return OilSuccess;
}

OilResult TaskSystem::work_task(TaskScheduler * const scheduler, AffinityMask const workerAffinity)
{
	for(unsigned queueIndex = 0; queueIndex < scheduler->queueCount; ++queueIndex)
	{
		if(0 != (scheduler->queueAffinities[queueIndex] & workerAffinity))
		{
			Task* task = nullptr;
			bool popSucceeded = scheduler->queues[queueIndex].try_pop(1, &task);
			if(false == popSucceeded)
			{
				continue;
			}
			else
			{
				OILassert(task, "what?");
				if(0 == task->taskData.pendingDependencies.load())
				{
					task->taskData.kernel(task->parameterStorage);

					uint32_t generation = generationFromTaskId(task->taskData.id);
					uint32_t index = indexFromTaskId(task->taskData.id);
					task->taskData.id = makeTaskId(index, generation + 1);

					if(task->taskData.successors.count > 0)
					{
						std::lock_guard<std::mutex> const lg(scheduler->successorMutex);

						std::uint32_t const offset = task->taskData.successors.offset;
						for(std::uint32_t i = 0; i < task->taskData.successors.count; ++i)
						{
							std::uint32_t const byteIndex = ((offset + i) % scheduler->successorStorageSize) / 8;
							std::uint32_t const bitIndex = (offset + i) - byteIndex;

							TaskId followUpTaskId = scheduler->successorStorage[offset + i].followUpTask;

							--(scheduler->taskStorage[indexFromTaskId(followUpTaskId)].taskData.pendingDependencies);

							scheduler->successorPendingFlags[byteIndex] &= ~(1 << bitIndex);
						}

						for(std::uint32_t i = offset / 8; ; ++i)
						{
							if(scheduler->successorPendingFlags[offset] == 0 &&
							  (scheduler->pendingSuccesorStart < scheduler->pendingSuccessorEnd))
							{
								scheduler->pendingSuccesorStart += 8;
							}
							else
							{
								break;
							}
						}
					}

					while(false == scheduler->freeTasks.try_push(1, &task->taskData.id))
					{
					}
					return OilSuccess;
				}
				else
				{
					while(false == scheduler->queues[queueIndex].try_push(1, &task))
					{
					}
				}
			}
		}
	}

	return OilError;
}

void TaskSystem::shut_down(TaskScheduler* scheduler)
{
	for(std::uint32_t workerIndex = 0; workerIndex < scheduler->workerCount; ++workerIndex)
	{
		scheduler->workers[workerIndex].stop();
	}

	for(std::uint32_t workerIndex = 0; workerIndex < scheduler->workerCount; ++workerIndex)
	{
		while(false == scheduler->workers[workerIndex].stopped())
		{

		}
	}

	return;
}

OilBool TaskSystem::is_task_completed(TaskScheduler const * const scheduler, TaskId const taskId)
{
	if(nullptr == scheduler) //TODO: move this to error checking layer?
	{
		return OIL_TRUE;
	}

	uint32_t const index = indexFromTaskId(taskId);

	if(index >= scheduler->taskCount)
	{
		return OIL_FALSE;
	}

	Task* task = &scheduler->taskStorage[index];

	if(generationFromTaskId(task->taskData.id) == generationFromTaskId(taskId))
	{
		return OIL_FALSE;
	}

	return OIL_TRUE;
}

size_t TaskSystem::get_task_parameter_storage_size()
{
	return Task::kParameterStorageSize;
}

uint32_t TaskSystem::indexFromTaskId(TaskId id)
{
	return id >> 8;
}

uint32_t TaskSystem::generationFromTaskId(TaskId id)
{
	return id & 255;
}

TaskId TaskSystem::makeTaskId(uint32_t index, uint32_t generation)
{
	constexpr uint32_t indexBits = 24;
	constexpr uint32_t idBits = sizeof(TaskId) * 8;

	uint32_t indexMasked = index << (idBits - indexBits);

	constexpr uint32_t generationMask = 255;
	uint32_t generationMasked = generation & generationMask;

	return indexMasked | generationMasked;
}

}
}



