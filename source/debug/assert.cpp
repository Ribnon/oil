extern "C"
{
#include <oil/external/debug/assert.h>
}

#if OIL_PLATFORM_WINDOWS

#include <Windows.h>

void oil_assert_failure(char const * const file, unsigned line, const char * const condition, const char * const message)
{
	(void)file;
	(void)line;
	(void)condition;
	(void)message;
	DebugBreak();
}

#else // OIL_PLATFORM_WINDOWS

#include <pthread.h>
#include <signal.h>

void oil_assert_failure(char const * const file, unsigned line, const char * const condition, const char * const message)
{
	(void) file;
	(void) line;
	(void) condition;
	(void) message;
	pthread_kill(pthread_self(), SIGINT);
}

#endif // OIL_PLATFORM_WINDOWS

