##oil

A selection of low level components you would need for building a game or game engine.

#Building
The oil components are build with CMake 3.8, requiring nothing but a C++11 compiler.

##components:

#oil::task
A task system implementation with a C99 interface. `oil::task` runs in fixed sized memory buffer supplied by the user and does not require runtime allocations at all.

Interface: `<oil/external/task/TaskSystem.h>`

Library: `oil::task`

##supported platforms:
Currently oil is supported on Linux and Windows. OSX/macOS should work as well, but it has never been tested.