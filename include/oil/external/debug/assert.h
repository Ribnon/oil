#ifndef OIL_DEBUG_ASSERT_H
#define OIL_DEBUG_ASSERT_H

#ifdef __cplusplus
extern "C" {
#endif


void oil_assert_failure(char const * const file, unsigned line, char const * const condition, char const * const message);

#define OILassert(condition, message) \
	do { if (! ( condition ) ) {oil_assert_failure(__FILE__, __LINE__, #condition, message);} } while(0)


#ifdef __cplusplus
} // extern "C"
#endif


#endif // #ifndef OIL_DEBUG_ASSERT_H
