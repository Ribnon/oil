#ifndef OIL_TASK_TASKSCHEDULER_H
#define OIL_TASK_TASKSCHEDULER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>


typedef int OilBool;
#define OIL_FALSE 0
#define OIL_TRUE 1

typedef enum OilResult
{
	OilSuccess = 0,
	OilError = 1
} OilResult;


/// `AffinityMask`s are used for `Task`s and `WorkerThread`s. If a bitwise AND
/// between a worker's and a Task's AffinityMask yields `0` they are considered
/// incompatible, otherwise the WorkerThread may execute the Task.
typedef uint32_t AffinityMask;

/// The `WorkerThreadDescriptor` contains all information necessary for the
/// `TaskScheduler` to create a `WorkerThread`.
typedef struct WorkerThreadDescriptor
{
	AffinityMask affinityMask;
} WorkerThreadDescriptor;

/// The `TaskSystemDescriptor` contains all information necessary to create
/// a `TaskScheduler` and internal objects.
typedef struct TaskSystemDescriptor
{
	/// the total amount of tasks managed by the system
	uint64_t taskCount;

	/// size of a single queue, there is one queue for each unique
	/// WorkerThread AffiniyMask.
	uint64_t queueSize;

	/// the amount of pending task inter-dependencies that can be managed by the
	/// system at any time.
	uint32_t concurrentPendingDependencyCount;

	uint32_t workerThreadCount;
	WorkerThreadDescriptor const * workerThreadDescriptions;
} TaskSystemDescriptor;

/// TaksId is an external handle to a given Task. The TaskId remains
/// valid indefinitly.
typedef uint32_t TaskId;

typedef void(*TaskKernel)(void* parameters);
typedef void(*Placeholder)();

typedef struct TaskDescriptor
{
	AffinityMask affinity;
	TaskKernel kernel;

	/// number of bytes that need to be supplied to the task
	size_t paramter_size;
	/// what parameters to supply to to the task
	void const * parameter_data;
	/// how many tasks depend on the execution of this one
	uint32_t successorCount;
	/// the TaksIds of dependend tasks
	TaskId const * successors;
} TaskDescriptor;

typedef struct TaskScheduler TaskScheduler;
typedef struct TaskSystem TaskSystem;
typedef struct Task Task;

/// obtains the task system interface from the runtime.
TaskSystem const* load_runtime_task_system(void const* runtime);

typedef struct TaskSystem
{
	/// calculates how many bytes of memory are required to construct a task system as
	/// described by taskSystemDescriptor.
	size_t (*get_system_memory_requirement) (TaskSystemDescriptor const * taskSystemDescriptor);

	/// constructs a task system into the given memory region, caller must
	/// guarantee that memory is large enough.
	TaskScheduler* (*create_task_system) (TaskSystemDescriptor const * taskSystemDescriptor, uint8_t* memory);

	/// allocates `taskCount` tasks and writes their ids to `ids` which
	/// transfers ownership over these tasks to the caller. Ownership of a
	/// `Task` can be passed back to the scheduler by enqueuing or releasing the
	/// `Task`.
	OilResult (*acquire_tasks) (TaskScheduler* scheduler, uint32_t taskCount, TaskId* ids);

	/// recycles `taskCount` tasks from `ids` without enqueueing the for
	/// execution.
	OilResult (*release_tasks) (TaskScheduler* scheduler, uint32_t taskCount, TaskId const* ids);

	/// submits `taskCount` Tasks from `ids` with their respective `descriptors`
	/// for execution. This transfers ownership over the submitted tasks back to
	/// the scheduler.
	OilResult (*enqueue_tasks) (TaskScheduler* scheduler, uint32_t taskCount, TaskId const* ids, TaskDescriptor const* descriptors);

	/// executes a task compatible with the `workerAffinity`.
	/// This will be called internally by `WorkerThread`s but also provides a
	/// way of running tasks on threads which are not a `WorkerThread`.
	/// Returns true if a `Task` was executed, false indicates not `Task` with
	/// matching affinity was found.
	OilResult (*work_task) (TaskScheduler* scheduler, AffinityMask workerAffinity);

	/// returns `false` if the Task referenced by taskId is still queued for
	/// execution. This function considers only tasks enqueued to `scheduler`,
	/// if `taskId` was enqueued on a different TaskScheduler this might return
	/// a false positive.
	OilBool (*is_task_completed) (TaskScheduler const* scheduler, TaskId taskId);

	/// returns how many bytes of parameter storage space a `Task` provides.
	size_t (*get_task_parameter_storage_size) ();

	/// shuts down the system, frees up all resources before returning from this
	/// function. This does not guarantee that all enqueued tasks have
	/// completed.
	void (*shut_down) (TaskScheduler* scheduler);

	Placeholder reserved[23];
} TaskSystem;

#ifdef __cplusplus
} // extern "C"
#endif

#endif // #ifndef OIL_TASK_TASKSCHEDULER_H

