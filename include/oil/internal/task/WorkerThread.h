#ifndef OIL_TASK_WORKERTHREAD_H
#define OIL_TASK_WORKERTHREAD_H

#include <cstdint>
#include <atomic>
#include <thread>

struct TaskScheduler;
struct TaskSystem;

namespace oil {
namespace task {

class WorkerThread
{
public:
	WorkerThread(TaskSystem const & system, TaskScheduler& scheduler, uint32_t const affinityMask);
	~WorkerThread();

	void stop();
	bool stopped() const;

private:
	void work_();

	static void threadEntryPoint_(WorkerThread* worker);

	TaskSystem const& system_;
	TaskScheduler& scheduler_;
	std::uint32_t filterMask_;

	std::atomic<std::uint8_t> run_;

	std::thread thread_;
};

}
}

#endif // #ifndef OIL_TASK_WORKERTHREAD_H
