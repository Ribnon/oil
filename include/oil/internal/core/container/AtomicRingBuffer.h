#include <atomic>

namespace oil
{
namespace core
{
namespace container
{

/// The AtomicRingBuffer is a fixed size FIFO queue designed for concurrent
/// access. Does not allocate memory itself at all.
/// It is highly recommended to chose unsigned integer types for `counter_type`
/// as otherwise undefined behaviour may occur after an overflow.
template < typename value_type, typename counter_type = uint64_t >
class AtomicRingBuffer
{
public:
	/// constructs a new AtomicRingBuffer with a capacity of `elementCount`
	/// using `buffer` for storage.
	AtomicRingBuffer(value_type* buffer, counter_type elementCount)
		:base_(buffer)
		,size_(elementCount)
		,readStart_(0)
		,readEnd_(0)
		,writeStart_(0)
		,writeEnd_(0)
	{

	}

	/// try to insert `elementCount` elements at the end of the queue.
	/// returns `true` if insertion succeeded.
	/// returns `false` if the queue had not enough capacity left.
	bool try_push(counter_type const elementCount, value_type const * const elements)
	{
		counter_type slotCandidate;
		do
		{
			slotCandidate = this->writeStart_.load();
			counter_type const writable = this->size_ + this->readEnd_.load() - slotCandidate;
			if(elementCount > writable)
			{
				return false;
			}

		} while(false == this->writeStart_.compare_exchange_weak(slotCandidate, slotCandidate + elementCount));

		counter_type slot = slotCandidate;

		for(counter_type i = 0; i < elementCount; ++i)
		{
			base_[(slot + i) % size_] = elements[i];
		}

		while(false == this->writeEnd_.compare_exchange_weak(slot, slot + elementCount) )
		{

		}

		return true;
	}

	/// try to extract `elementCount` items from the start of the queue.
	/// returns `true` if extraction succeded.
	/// returns `false` if less than `elementCount` elements were in the queue.
	bool try_pop(counter_type const elementCount, value_type* const elements)
	{
		counter_type slotCandidate;
		do
		{
			slotCandidate = this->readStart_;
			counter_type const readable = this->writeEnd_.load() - slotCandidate;
			if(elementCount > readable)
			{
				return false;
			}

		} while(false == this->readStart_.compare_exchange_weak(slotCandidate, slotCandidate + elementCount));

		counter_type slot = slotCandidate;

		for(counter_type i = 0; i < elementCount; ++i)
		{
			elements[i] = base_[(slot + i) % this->size_];
		}

		while(false == this->readEnd_.compare_exchange_weak(slot, slot + elementCount) )
		{

		}

		return true;
	}

private:
	value_type* const base_;
	counter_type const size_;

	std::atomic< counter_type > readStart_;
	std::atomic< counter_type > readEnd_;

	std::atomic< counter_type > writeStart_;
	std::atomic< counter_type > writeEnd_;
};

}
}
}


