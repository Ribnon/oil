#!/bin/bash

apt-get update
apt-get install -y x11-xserver-utils

mkdir -p build-env/tmp
curl https://cmake.org/files/v3.8/cmake-3.8.2-Linux-x86_64.tar.gz -o build-env/tmp/cmake-3.8.2.tar.gz
cd build-env
tar -xf tmp/cmake-3.8.2.tar.gz
export PATH=`pwd`/cmake-3.8.2-Linux-x86_64/bin/:$PATH
cd ..
